package repository

import "github.com/jinzhu/gorm"

const BaseResponseRowsPerPage = 10

type Repository struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		DB: db,
	}
}
