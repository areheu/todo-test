package repository

import (
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func createNewRepository(t *testing.T) *Repository {
	repository := NewRepository(TestDB)
	assert.IsType(t, &Repository{}, repository)
	assert.IsType(t, &gorm.DB{}, repository.DB)

	return repository
}

func TestRepository_NewRepository(t *testing.T) {
	createNewRepository(t)
}
