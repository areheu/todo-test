package repository

import (
	"toDo/model"
)

func (r *Repository) GetTodoByID(id int) (todo model.Todo, err error) {
	err = r.DB.First(&todo, "id = ?", id).Error
	return
}

func (r *Repository) GetTodoList(params model.GetTodoListParams) (todoList []model.Todo, err error) {
	req := r.DB.Model(&model.Todo{})

	limit := BaseResponseRowsPerPage
	if params.Limit != 0 {
		limit = params.Limit
	}

	offset := 0
	if params.Page != 0 {
		offset = (params.Page - 1) * limit
	}

	err = req.Offset(offset).Limit(limit).Order("id desc").Find(&todoList).Error
	return
}

func (r *Repository) SaveTodo(todo model.Todo) (todoRes model.Todo, err error) {
	todoRes.UserID = todo.UserID
	todoRes.Title = todo.Title
	todoRes.Completed = todo.Completed

	err = r.DB.Create(&todoRes).Error
	return
}

func (r *Repository) UpdateTodo(todo model.Todo) (model.Todo, error) {
	err := r.DB.Model(&model.Todo{}).
		Where("id = ?", todo.ID).
		Save(todo).Error

	return todo, err
}

func (r *Repository) DeleteTodoByID(id int) (err error) {
	err = r.DB.Delete(&model.Todo{}, "id = ?", id).Error
	return
}
