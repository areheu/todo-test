package main

import (
	"toDo/db"
	"toDo/handler"
	"toDo/repository"
	"toDo/service"

	_ "toDo/docs"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	echoSwagger "github.com/swaggo/echo-swagger"
)

func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

// @title ToDo API
// @version 0.1
// @contact.email areheu@gmail.com
// @BasePath /
func main() {
	_db := db.Connect()
	defer func(_db *gorm.DB) {
		_ = _db.Close()
	}(_db)
	_repository := repository.NewRepository(_db)
	_service := service.NewService(_repository)
	_handler := handler.NewHandler(_service)

	e := echo.New()

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	// todo endpoints
	{
		todoGroup := e.Group("todos")
		todoGroup.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
			Format: "${time_rfc3339_nano}: ${method} ${remote_ip}${uri}; Status:${status}; Time:${latency_human}; In:${bytes_in}b; Out:${bytes_out}b;\n",
		}))

		todoGroup.GET("", _handler.GetTodoList)
		todoGroup.GET("/:id", _handler.GetTodoByID)
		todoGroup.POST("", _handler.SaveTodo)
		todoGroup.PUT("", _handler.UpdateTodo)
		todoGroup.DELETE("/:id", _handler.DeleteTodoByID)
	}

	e.Logger.Fatal(e.Start(":8080"))
}
