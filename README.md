### Usage:

1. Copy .env.example to .env and set correct variables (TEST_DB will be used for tests)
2. Run dockers:
```
docker-compose up -d
```

### App base url
http://localhost:8080

### Swagger base url
http://localhost:8080/swagger/index.html

### Swagger docs generation

```
swag init
```


### Self-check

#### Linting:

```
golangci-lint run
```


#### Tests:

```
docker exec -it golang_todo_app go test ./...
```

