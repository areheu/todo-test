package model

type Todo struct {
	ID        int    `json:"id" gorm:"primary_key"`
	UserID    int    `json:"userId"`
	Title     string `json:"title,omitempty"`
	Completed bool   `json:"completed"`
}

type GetTodoListParams struct {
	Limit int `query:"limit"`
	Page  int `query:"page"`
}
