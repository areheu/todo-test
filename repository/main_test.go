package repository

import (
	"fmt"
	"os"
	"testing"
	"toDo/model"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
)

const (
	dbDriver = "postgres"
)

var (
	TestDB         *gorm.DB
	TestRepository *Repository
)

func TestMain(m *testing.M) {
	if err := godotenv.Load("../.env"); err != nil {
		log.Fatal("cannot find .env file:", err)
	}

	dbUser := os.Getenv("TEST_DB_USER")
	dbPassword := os.Getenv("TEST_DB_PASSWORD")
	dbName := os.Getenv("TEST_DB_NAME")
	dbHost := os.Getenv("TEST_DB_HOST")
	dbPort := os.Getenv("TEST_DB_PORT")
	dbSource := fmt.Sprintf("postgresql://%s:%s@%s:%s", dbUser, dbPassword, dbHost, dbPort)

	db, err := gorm.Open(dbDriver, dbSource+"?sslmode=disable")
	if err != nil {
		log.Fatal("cannot open connection to db:", err)
	}

	err = db.Exec("DROP DATABASE IF EXISTS " + dbName).Error
	if err != nil {
		log.Fatal("cannot drop test db:", err)
	}

	err = db.Exec("CREATE DATABASE " + dbName).Error
	if err != nil {
		log.Fatal("cannot create test db:", err)
	}
	_ = db.Close()

	TestDB, err = gorm.Open(dbDriver, dbSource+"/"+dbName+"?sslmode=disable")
	if err != nil {
		log.Fatal("cannot connect to db:", err)
	}

	TestDB.AutoMigrate(
		&model.Todo{},
	)

	TestRepository = createNewRepository(&testing.T{})

	exitCode := m.Run()
	_ = TestDB.Close()

	os.Exit(exitCode)
}
