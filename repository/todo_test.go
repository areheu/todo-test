package repository

import (
	"testing"
	"toDo/model"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
)

var todoItemList = []model.Todo{
	{
		UserID:    1,
		Title:     "Task1",
		Completed: false,
	},
	{
		UserID:    1,
		Title:     "Task2",
		Completed: true,
	},
	{
		UserID:    2,
		Title:     "UrgentTask",
		Completed: false,
	},
	{
		UserID:    2,
		Title:     "UrgentTask - done",
		Completed: true,
	},
}

func TestSaveTodo(t *testing.T) {
	res, err := TestRepository.SaveTodo(todoItemList[0])

	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.IsType(t, model.Todo{}, res)
	require.Equal(t, todoItemList[0].UserID, res.UserID)
	require.Equal(t, todoItemList[0].Title, res.Title)
	require.Equal(t, todoItemList[0].Completed, res.Completed)
	require.NotZero(t, res.ID)
}

func TestGetTodoByID(t *testing.T) {
	todo, err := TestRepository.SaveTodo(todoItemList[1])
	require.NoError(t, err)

	res, err := TestRepository.GetTodoByID(todo.ID)
	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.IsType(t, model.Todo{}, res)
	require.Equal(t, todo.ID, res.ID)
	require.Equal(t, todo.UserID, res.UserID)
	require.Equal(t, todo.Title, res.Title)
	require.Equal(t, todo.Completed, res.Completed)
}

func TestUpdateTodo(t *testing.T) {
	todo, err := TestRepository.SaveTodo(todoItemList[2])
	require.NoError(t, err)

	todoItemList[3].ID = todo.ID
	res, err := TestRepository.UpdateTodo(todoItemList[3])
	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.IsType(t, model.Todo{}, res)
	require.Equal(t, todo.ID, res.ID)
	require.Equal(t, todoItemList[3].UserID, res.UserID)
	require.Equal(t, todoItemList[3].Title, res.Title)
	require.Equal(t, todoItemList[3].Completed, res.Completed)
}

func TestGetTodoList(t *testing.T) {
	var (
		err    error
		params model.GetTodoListParams
	)
	params.Limit = 3
	params.Page = 0

	for _, n := range todoItemList {
		_, err = TestRepository.SaveTodo(n)
		require.NoError(t, err)
	}

	res, err := TestRepository.GetTodoList(params)
	require.NoError(t, err)
	require.Len(t, res, 3)
	for _, todo := range res {
		require.NotEmpty(t, todo)
	}

	params.Limit = 2
	params.Page = 2
	res, err = TestRepository.GetTodoList(params)
	require.NoError(t, err)
	require.Len(t, res, 2)
	for _, todo := range res {
		require.NotEmpty(t, todo)
	}
}

func TestDeleteTodoByID(t *testing.T) {
	todo, err := TestRepository.SaveTodo(todoItemList[0])
	require.NoError(t, err)

	err = TestRepository.DeleteTodoByID(todo.ID)
	require.NoError(t, err)

	res, err := TestRepository.GetTodoByID(todo.ID)
	require.Error(t, err)
	require.EqualError(t, err, gorm.ErrRecordNotFound.Error())
	require.Empty(t, res)
}
