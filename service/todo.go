package service

import (
	"toDo/model"
)

func (s *Service) GetTodoByID(id int) (todo model.Todo, err error) {
	return s.Repository.GetTodoByID(id)
}

func (s *Service) GetTodoList(params model.GetTodoListParams) (todoList []model.Todo, err error) {
	return s.Repository.GetTodoList(params)
}

func (s *Service) SaveTodo(todo model.Todo) (model.Todo, error) {
	return s.Repository.SaveTodo(todo)
}

func (s *Service) UpdateTodo(todo model.Todo) (model.Todo, error) {
	return s.Repository.UpdateTodo(todo)
}

func (s *Service) DeleteTodoByID(id int) (err error) {
	return s.Repository.DeleteTodoByID(id)
}
