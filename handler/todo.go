package handler

import (
	"net/http"
	"strconv"
	"toDo/model"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

//GetTodoByID ..
// @Summary Get single ToDo by ID
// @Tags ToDos
// @Produce json
// @Param id path int true "Id of ToDo"
// @Success 200 {object} model.Todo
// @Router /todos/{id} [Get]
func (h *Handler) GetTodoByID(c echo.Context) error {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		c.Logger().Errorf("GetTodoByID. Cannot parse id value: %s", idParam)
		return c.JSON(http.StatusBadRequest, "parameter 'id' must be integer")
	}

	todo, err := h.Service.GetTodoByID(id)
	if err == gorm.ErrRecordNotFound {
		return c.JSON(http.StatusNoContent, nil)
	}
	if err != nil {
		c.Logger().Errorf("GetTodoByID. Error getting ToDo from DB: %v", err)
		return c.JSON(http.StatusInternalServerError, "error getting ToDo from DB")
	}

	return c.JSON(http.StatusOK, todo)
}

//GetTodoList ..
// @Summary Get list of ToDos
// @Tags ToDos
// @Produce json
// @Param limit query number false "Limit for returning records"
// @Param page query number false "Page of records"
// @Success 200 {array} []model.Todo
// @Router /todos [Get]
func (h *Handler) GetTodoList(c echo.Context) error {
	var params model.GetTodoListParams

	err := echo.QueryParamsBinder(c).
		Int("page", &params.Page).
		Int("limit", &params.Limit).
		BindError()

	if err != nil {
		c.Logger().Errorf("GetTodoList. Error binding parameters: %v", err)
		return c.JSON(http.StatusBadRequest, "error reading parameters: "+err.Error())
	}

	todoList, err := h.Service.GetTodoList(params)
	if err != nil {
		c.Logger().Errorf("GetTodoList. Error getting ToDo from DB: %v", err)
		return c.JSON(http.StatusInternalServerError, "error getting ToDo from DB")
	}

	return c.JSON(http.StatusOK, todoList)
}

//SaveTodo ..
// @Summary Insert Single ToDo
// @Tags ToDos
// @Produce json
// @Param InJSON body model.Todo true "model"
// @Success 200 {object} model.Todo
// @Router /todos [Post]
func (h *Handler) SaveTodo(c echo.Context) error {
	var (
		todo    model.Todo
		todoRes model.Todo
	)
	err := c.Bind(&todo)
	if err != nil {
		c.Logger().Errorf("SaveTodo. Error binding JSON: %v", err)
		return c.JSON(http.StatusBadRequest, "cannot process request")
	}

	todoRes, err = h.Service.SaveTodo(todo)
	if err != nil {
		c.Logger().Errorf("SaveTodo. Error inserting ToDo to DB: %v", err)
		return c.JSON(http.StatusInternalServerError, "error inserting ToDo to DB")
	}

	return c.JSON(http.StatusOK, todoRes)
}

//UpdateTodo ..
// @Summary Update Single ToDo
// @Tags ToDos
// @Produce json
// @Param InJSON body model.Todo true "model"
// @Success 200 {object} model.Todo
// @Router /todos [Put]
func (h *Handler) UpdateTodo(c echo.Context) error {
	var (
		todo    model.Todo
		todoRes model.Todo
	)
	err := c.Bind(&todo)
	if err != nil {
		c.Logger().Errorf("UpdateTodo. Error binding JSON: %v", err)
		return c.JSON(http.StatusBadRequest, "cannot process request")
	}

	if todo.ID == 0 {
		c.Logger().Infof("UpdateTodo. Error inserting ToDo to DB: %v", err)
		return c.JSON(http.StatusBadRequest, "id must be filled")
	}

	todoRes, err = h.Service.UpdateTodo(todo)
	if err != nil {
		c.Logger().Errorf("UpdateTodo. Error inserting ToDo to DB: %v", err)
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, todoRes)
}

//DeleteTodoByID ..
// @Summary Delete Single ToDo
// @Tags ToDos
// @Produce json
// @Param id path int true "Id of ToDo"
// @Success 200 {object} model.Todo
// @Router /todos/{id} [Delete]
func (h *Handler) DeleteTodoByID(c echo.Context) error {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		c.Logger().Errorf("DeleteTodoByID. Cannot parse id value: %s", idParam)
		return c.JSON(http.StatusBadRequest, "parameter 'id' must be integer")
	}

	err = h.Service.DeleteTodoByID(id)
	if err != nil {
		c.Logger().Errorf("DeleteTodoByID. Error deleting ToDo from DB: %v", err)
		return c.JSON(http.StatusInternalServerError, "error deleting ToDo from DB")
	}

	return c.JSON(http.StatusOK, "deleted successfully")
}
